# JB's Trivia Game

## Prerequisites

  - .NET Core 2.1 or higher

## Getting started

In a command prompt:

```
dotnet restore
dotnet build
dotnet run
```
## 

## Author

Justin Baker <me@jbaker.dev>

## License

There is none. Free as in speech, free as in beer.
